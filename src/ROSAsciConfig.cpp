#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>

#include "ROSApplication/ROSAsciConfig.h"
#include "DFSubSystemItem/SSIConfiguration.h"  // for SSIConfiguration
#include "DFThreads/DFCountedPointer.h"        // for DFCountedPointer

using namespace ROS;

void ROSAsciConfig::loadConfig(std::string name, DFCountedPointer<Config> configuration)
{
   std::string configDir;
   char* configDirfromEnvironment=getenv("DAQ_CONFIG_DIR");
   if (configDirfromEnvironment == 0) {
     std::cout << "DAQ_CONFIG_DIR is not set." << std::endl;
     std::exit (EXIT_FAILURE);
   }
   else {
      configDir=configDirfromEnvironment;
   }
   std::string fileName=configDir + "/" + name;
   std::ifstream configFile(fileName.c_str());
   if (!configFile) {
      std::cerr << "cannot open " << fileName
           << " iostream has no error reporting so can't tell you why!\n";
      std::exit (EXIT_FAILURE);
   }

   const int lineSize = 1024;
   char buf[lineSize];
   
   while(configFile.getline(buf, lineSize)) {
     if (buf[0] == '#') { // skip comment lines
       continue;
     }
     std::string line(buf);
     int equalsPosition=line.find('=');
     std::string key(line, 0, equalsPosition);
     line.erase(0, equalsPosition+1);
     std::string value(line);
     configuration->set(key, value);
   }
}

ROSAsciConfig::~ROSAsciConfig() { 
}

SSIConfiguration * ROSAsciConfig::update() {

  SSIConfiguration* ssiConf = new SSIConfiguration();
  DFCountedPointer<Config> corePairs = Config::New();  
  loadConfig("coreConfig.dat", corePairs);
  Config::T_ConfigContainer coreConfig;
  coreConfig.push_back(corePairs);
  ssiConf->setConfig("Core", coreConfig);
  
  DFCountedPointer<Config> trgPairs = Config::New();  
  loadConfig("triggerInConfig.dat", trgPairs);
  Config::T_ConfigContainer trgConfig;
  trgConfig.push_back(trgPairs);
  ssiConf->setConfig("TriggerIn", trgConfig);
  
  DFCountedPointer<Config> outPairs = Config::New();
  loadConfig("dataOutConfig.dat", outPairs);
  Config::T_ConfigContainer outConfig;
  outConfig.push_back(outPairs);
  ssiConf->setConfig("DataOut", outConfig);
  
  int numberOfReadoutModules = corePairs->getInt("numberOfReadoutModules");
  Config::T_ConfigContainer rmConfig;
  for (int i=0 ; i< numberOfReadoutModules; i++) {
    DFCountedPointer<Config> rmPairs = Config::New();  
    // Load generic ReadoutModule configuration file
    loadConfig("readoutModuleConfig.dat", rmPairs);
    // Now the specific file for this ReadouModule
    std::ostringstream filename;
    filename << "readoutModuleConfig_" << i<< ".dat";
    loadConfig(filename.str(), rmPairs);
    rmConfig.push_back(rmPairs);
  }
  ssiConf->setConfig("ReadoutModule", rmConfig);
  return ssiConf;
}
  
SSIConfiguration * ROSAsciConfig::updateRun() {

  SSIConfiguration* ssiRunConf = new SSIConfiguration();
  DFCountedPointer<Config> runPairs = Config::New();  
  loadConfig("runparams.dat", runPairs);
  Config::T_ConfigContainer runConfig;
  runConfig.push_back(runPairs);
  ssiRunConf->setConfig("RunParameters", runConfig);
  
  return ssiRunConf;
}


/** Shared library entry point */
extern "C" {
  extern IOManagerConfig* createROSAsciConfig(void* rosName);
}

IOManagerConfig* createROSAsciConfig(void* rosName) {
  return (new ROSAsciConfig());
}
