/*
Configuration class extension as RCD example
Author: E. Pasqualucci, G. Lehmann
*/

#include "DFdal/CircDataOut.h"
#include "DFdal/SLinkDataOut.h"

#include "ROSApplication/RCDConfig.h"

#include "DFSubSystemItem/Config.h"      // for Config, ROS
#include "DFThreads/DFCountedPointer.h"  // for DFCountedPointer
#include "DFdal/ReadoutOutput.h"         // for ReadoutOutput
#include "DFdal/ReadoutTrigger.h"        // for ReadoutTrigger
#include "ROSApplication/ROSDBConfig.h"  // for ROSDBConfig

using namespace ROS;

RCDConfig::RCDConfig(char* name) : ROSDBConfig(name){}

void RCDConfig::getOutputChannel(const daq::df::ReadoutOutput * pOut,
				 DFCountedPointer<Config> outPairs) {

  if(const daq::df::CircDataOut * eDo = 
     m_confDB->cast<daq::df::CircDataOut, daq::df::ReadoutOutput>(pOut)) {
    outPairs->set("outputCircName", eDo->get_OutputCircName ());
    outPairs->set("bufSize", eDo->get_BufferSize ());
    outPairs->set("samplingGap", eDo->get_SamplingGap ());
    outPairs->set("outputDelay", eDo->get_OutputDelay ());
    outPairs->set("throwIfFull", eDo->get_ThrowIfFull ());
  }
  else if(const daq::df::SLinkDataOut * sDo = 
	  m_confDB->cast<daq::df::SLinkDataOut, daq::df::ReadoutOutput>(pOut)){	  
    outPairs->set("occurrence", sDo->get_SLinkOccurrence());
  }
  
  else {
    ROSDBConfig::getOutputChannel(pOut, outPairs);
  }
}

void RCDConfig::getModule(const daq::df::ReadoutModule* rom,
			  DFCountedPointer<Config> moduleConfig) {
  ROSDBConfig::getModule(rom, moduleConfig);
}

DFCountedPointer<Config>
RCDConfig::getTrigger (const daq::df::ReadoutTrigger * trg) {

  // Get the right TriggerIn  

  if (trg->class_name() == "NullTriggerIn") {

    DFCountedPointer<Config> trgPairs = Config::New();

    return trgPairs;
  }
  else {
    return ROSDBConfig::getTrigger (trg);
  }
}

/** Shared library entry point */
extern "C" {
  extern IOManagerConfig* createRCDConfig(void* name);
}

IOManagerConfig* createRCDConfig(void* name) {
  return (new RCDConfig((char*) name));
}
