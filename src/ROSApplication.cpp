//	Example controlled Process MAIN 
//
// G. Lehmann, August 2002

#include <sched.h>
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <string>

#include <sys/resource.h>

#include <boost/program_options.hpp>

#include "TThread.h"
#include "TH1.h"
#include "TSystem.h"

#include "ers/ers.h"

#include "ROSCore/IOManager.h"
#include "ROSCore/IOManagerConfig.h"

#include "rcc_time_stamp/tstamp.h"
#include "ROSApplication/ApplicationException.h"
#include "ROSUtilities/ROSErrorReporting.h"

#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/ItemCtrl/ItemCtrl.h"

using namespace ROS;



int main (int argc, char* argv[]) {

  // Get commandline parameters
   bool interactiveMode;
   std::string myName;
   std::string parent;
   std::string partition;
   std::string segment;
   std::string database;
   std::string configName;
   std::int64_t cpuMask;
   std::uint32_t memoryLimit;
   std::string options;
   namespace po = boost::program_options;
   po::options_description desc("This program is the Readout data taking application.");
   desc.add_options()
      ("interactive,i",po::bool_switch(&interactiveMode), "Turn on interactive control mode")
      ("name,n", po::value<std::string>(&myName)->default_value(""), "Name of this object in OKS database")
      ("configuration,c", po::value<std::string>(&configName)->default_value("ROSDBConfig"), "name of configuration plugin")
      ("affinity,a",po::value<std::int64_t>(&cpuMask)->default_value(0), "set affinity to CPU(s) in this mask")
      ("maxMemory,m",po::value<std::uint32_t>(&memoryLimit)->default_value(0), "limit on memory allocation in MB")
      ("options,o", po::value<std::string>(&options)->default_value(""), "Arbitrary global options")

      ("partition,p", po::value<std::string>(&partition)->default_value(""), "Name of the partition in OKS database")
      ("segment,s", po::value<std::string>(&segment)->default_value(""), "Name of the segment in OKS database")
      ("parent,P", po::value<std::string>(&parent)->default_value(""), "Name of the parent controller object in OKS database")
      ("database,d", po::value<std::string>(&database)->default_value(""), "Name of the OKS database")
      ;
   try {
      po::variables_map vm;
      po::store(po::parse_command_line(argc, argv, desc), vm);
//      po::store(po::command_line_parser(argc, argv).options(desc).allow_unregistered().run(), vm);
      po::notify(vm);
   }
   catch (std::exception& ex) {
      std::cerr << desc << std::endl;
     CREATE_ROS_EXCEPTION(appEx, ApplicationException, BAD_CMDLINE, "" );
     ers::fatal(appEx);
   }

   std::cout << "Starting " << myName << std::endl;



   // Remove ROOT SigHandlers
   for (int sig = 0; sig < kMAXSIGNALS; sig++) {
     gSystem->ResetSignal((ESignals)sig);
   }
   // Prevent histogram tracking
   TH1::AddDirectory(kFALSE);
   // Initialise mutexes
   TThread::Initialize(); 


   if (cpuMask != -1) {
      cpu_set_t pmask;
      CPU_ZERO(&pmask);
      if (cpuMask == 0) {
         CPU_SET(cpuMask,&pmask);
      }
      else {
         for (int cpu=0; cpu<64; cpu++) {
            std::uint64_t bitField=(uint64_t)1<<cpu;
            if ((cpuMask&bitField) == bitField) {
               CPU_SET(cpu,&pmask);
            }
         }
      }
      int affStatus=sched_setaffinity(0, sizeof(pmask), &pmask);
      if (affStatus != 0) {
         CREATE_ROS_EXCEPTION(ex, ApplicationException, BAD_AFFINITY, "  cpumask "  << cpuMask << std::hex << " (0x" << cpuMask << ")" );
         ers::fatal(ex);
      }
   }

   if (memoryLimit != 0) {
      struct rlimit limit;
      getrlimit(RLIMIT_AS,&limit);
      limit.rlim_cur=memoryLimit*1024*1024;
      setrlimit(RLIMIT_AS,&limit);
   }

   ts_open(1, TS_DUMMY);	// for online (ts_wait)
   TS_OPEN(100000, TS_H1);	// for (optional:TSTAMP) time stamping

   std::shared_ptr<IOManager> iomanager=std::make_shared<IOManager>(myName);

  if(interactiveMode) {
    ts_open(1, TS_DUMMY);	// for online (ts_wait)
    TS_OPEN(100000, TS_H1);	// for (optional:TSTAMP) time stamping
    TS_START(TS_H1);
    TS_RECORD(TS_H1,1);
    
    setenv("TDAQ_DB_IMPLEMENTATION", "OKS", 1);
  }

  IOManagerConfig::globalOptions(options);

  iomanager->setConfigPlugin(configName);

  iomanager->setInteractive(interactiveMode);



  // Create the Control Thread
  daq::rc::CmdLineParser parser(argc,argv,false);
  daq::rc::ItemCtrl myItem(parser, iomanager); 
  myItem.init();
  myItem.run();


  if(interactiveMode) {
    TS_SAVE(TS_H1, "ROS_timing");
    TS_CLOSE(TS_H1);
    ts_close(TS_DUMMY);
  }
  std::cout << "Exiting from " << myName << std::endl;
  /* 
     08 November 2016
     The next line of code does not really belong here any more.
     On the other hand, its historical value prevents us from
     deleting it at this stage. This note is left as a reminder
     for future coders. (ADHI-4255)
  */
  // std::exit (0);
}
