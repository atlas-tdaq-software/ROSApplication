// $Id$
//
// $Log$
// Revision 1.110  2009/02/25 17:46:05  gcrone
// Get DC NodeIDs from partiton get_node_id method for both ROS and any Robins
//
// Revision 1.109  2009/02/02 18:04:27  gcrone
// Try to get the network address of the Robin from its HasNetworkInterface relationship
//
// Revision 1.108  2009/01/23 09:19:20  gcrone
// Update for new dal disabled() method which only takes one arg
//
// Revision 1.107  2008/11/21 13:32:52  gcrone
// Fix configuration of DataOuts when defaulting DebugDataOut but
// specifying MonitoringDataOut
//
// Revision 1.106  2008/11/14 14:56:04  gcrone
// Handle a ROS without ReceiveMulticast set properly.
//
// Revision 1.105  2008/11/13 15:17:04  gcrone
// Set T0ProjectTag to empty string when IS is not available.
//
// Revision 1.104  2008/11/06 15:35:19  gcrone
// Extract T0 project tag from IS runparams.
//
// Revision 1.103  2008/10/31 17:13:24  gcrone
// Translate ReceiveMulticast to NetDeleteEnable for robin.
//
// Revision 1.102  2008/10/31 16:01:19  gcrone
// Pass global options from the command line
//
// Revision 1.101  2008/10/16 07:41:50  gcrone
// Remove obsolete include of DFdal/DCMulticastGroup.h.
// Add UID of each data channel to fmPairs.
//
// Revision 1.100  2008/10/03 13:58:30  gcrone
// Update for changes in DC multicast group configuration
//
// Revision 1.99  2008/09/29 13:29:22  gcrone
// Update to comply with new ConfigObject interface.
//
// Revision 1.98  2008/05/19 16:57:06  gcrone
// Make a default EMonDataOut for the DebugOutput if none is defined.
//
// Revision 1.97  2008/05/13 08:29:37  gcrone
// Protect against following an empty Configuration relationship
//
// Revision 1.96  2008/04/22 15:16:43  gcrone
// Make sure numberOfChannels is set correctly regardless of mixture of
// InputChannels and other types of Resource.  Set channelClass for all
// channels so that they can be distinguished in a heterogenous
// configuration.
//
// Revision 1.95  2008/04/22 12:52:18  gcrone
// Do check for disabled ReadoutModule on device which is a ResourceBase
// pointer, not on resources which is cast to a ResourceSet since a
// ReadoutModule may be a simple Resource!
//
// Revision 1.94  2008/03/28 11:05:24  gcrone
// Parse attributes for all Resources Contained by a ReadoutModule, even
// the ones that aren't InputChannels.
//
// Revision 1.93  2008/03/26 19:37:54  gcrone
// New inheritance for ReadoutModules
//
// Revision 1.92  2008/03/17 12:23:11  gcrone
// Bug fix: index should be declared outside the loop over DataOuts
//
// Revision 1.91  2008/03/10 11:03:48  gcrone
// Supply default EMonDataOut configuration for monitoring output if none specified in database
//
// Revision 1.90  2008/03/07 16:19:17  gcrone
// Allow ReadoutModule to contain Resources other than InputChannel
//
// Revision 1.89  2008/02/26 09:08:43  gcrone
// Add support for single fragment mode data driven trigger
//
// Revision 1.88  2008/02/25 15:05:53  gcrone
// New multiple DataOut scheme.
//
// Revision 1.87  2008/02/20 08:19:40  gcrone
// File writing now done by FileDataOut instead of EmulatedDataOut.
// Don't invent a DataOut if there's none specified in the database.
//
// Revision 1.86  2008/02/06 11:04:30  gcrone
// New recording_enabled in RunParams
//
// Revision 1.85  2007/10/18 14:03:44  gcrone
// Support different sized signed integers properly including 64bit
//
// Revision 1.84  2007/09/04 17:05:08  gcrone
//   Allow ReadoutModule's Contains relationship to point to a ResourceSet
// or and InputChannel using code based on patch from Fatih.
//   Also fix a bug introduced with the change to disabled logic -
// push_back of fm_pairs was wrongly inside the loop over InputChannels!!
//
// Revision 1.83  2007/07/12 17:21:40  gcrone
// Handle disabled Readoutmodules as well as Input Channels.
//
// Revision 1.82  2007/07/10 16:00:27  gcrone
// Call disabled method of input channel rather than getting list of
// disabled components and comparing UIDs.
//
// Revision 1.81  2007/06/19 16:00:05  gcrone
// Use the UID of the MulticastGroup from the database instead of hardcoded ROS
//
// Revision 1.80  2007/04/25 15:46:00  gcrone
// Expert parameters now belong to the Configuration of
// RobinReadoutModule/DataChannel not directly to the Module/Channel.
//
// Revision 1.79  2007/04/25 07:38:36  gcrone
// Change channelId to ChannelId for ROBIN
//
// Revision 1.78  2007/04/19 12:39:10  gcrone
// Include TESTNODEID in list of remote apps for DC messaging.  Derive communicationTimeout from ActionTimeout
//
// Revision 1.77  2007/04/18 08:32:33  gcrone
// NumberOfRequestHandlers now starts with uppercase N!!
//
// Revision 1.76  2007/04/17 15:10:23  gcrone
// Convert PhysAddress to PhysicalAddress in getModule()
//
// Revision 1.75  2007/04/13 14:25:07  gcrone
// Merge in changes from nodeid branch
//
// Revision 1.74  2007/04/02 11:19:18  gcrone
// Bug fix - In getAllAttributes(), pass the index argument on when it
// calls itself!
//
// Revision 1.73  2007/03/27 12:54:14  gcrone
// Major update tom fit in with new schema.  the getAllAttributes method
// now automatically follows any relationship called Configuration and
// inserts its attributes into the current Config object.  Also, changed
// all ers messages to use ApplicationException.
//
// Revision 1.72  2007/02/14 15:53:18  gcrone
// Pass partition name in core Config.
//
// Revision 1.71  2006/10/31 17:24:12  gcrone
// New ROSException stuff
//
// Revision 1.70  2006/09/04 15:42:56  gcrone
// IS methods now throw exceptions rather than returning a status.
//
// Revision 1.69  2006/08/18 12:07:40  gcrone
// Use ROS namespace again.
//
// Revision 1.68  2006/08/11 15:26:59  gcrone
// Comment out use of ROS namespace for now
//
// Revision 1.67  2006/08/10 15:34:08  gcrone
// Remove warning when UserMetaData not found in RunParams.
//
// Revision 1.66  2006/07/27 09:59:35  gcrone
// Database get() and get_class_info() no longer return a bool status.
//
// Revision 1.65  2006/07/06 15:28:21  gcrone
// Fix getting of UserMetaData from IS.
//
// Revision 1.64  2006/06/30 07:13:20  gcrone
// Idiot:  forgot the get_ prefix on ScheduledMonitoringAction Name and Interval!!
//
// Revision 1.63  2006/06/29 14:45:02  gcrone
// Get scheduled monitoring action info for corePairs.
// Also, give an error if we can't determine the multicast address for
// the ROS when using message passing.
//
// Revision 1.62  2006/05/12 11:31:45  gcrone
// Fix internal default EmulatedDataOut, use key FormattedOutput not
// useEventStorage!
//
// Revision 1.60  2006/04/20 13:58:41  gcrone
// Force use of only 1 request handler if a data driven trigger is selected.
//
// Revision 1.59  2006/03/30 18:01:24  gcrone
// Attempt to get RunParams.UserMetaData from IS and store in runParams Config.
//
// Revision 1.58  2006/03/22 16:36:34  gcrone
// Hack for Preloaded DataChannels, pass module class name in fmPairs.
//
// Revision 1.57  2006/03/21 17:26:24  gcrone
// Get values for user command MemoryPool and add to corePairs
//
// Revision 1.56  2006/03/20 09:25:30  gcrone
// Call getAllAttributes for core params as well.
//
// Revision 1.55  2006/03/10 18:00:00  gcrone
// Get Application from dal instead of Application
//
// Revision 1.54  2006/01/17 15:38:56  gcrone
// Remove redundant include of dal/implementation.h (why didn't I read
// Igor's mail all the way through before the last commit?).
//
// Revision 1.53  2006/01/17 15:32:09  gcrone
// Remove redundant include of oksconfig/OksConfiguration.h
//
// Revision 1.52  2005/12/14 14:51:58  gcrone
// Remove reference to DFdal/ROSApplication.h which no longer exists.
//
// Revision 1.51  2005/12/13 14:30:54  gcrone
// Update for new df schema
//
// Revision 1.50  2005/11/23 18:07:01  gcrone
// Update for new df schema
//
// Revision 1.49  2005/11/08 16:58:20  gcrone
// One dbIOM->ReadoutModule had missed being changed to dbIOM->get_ReadoutModule.
//
// Revision 1.48  2005/11/06 17:26:35  gcrone
// Remove more redundant code (byteSwapping, channelPhysicalAddress,
// EmulatedReadoutModule)
//
// Revision 1.47  2005/11/03 17:01:07  gcrone
// Removed redundant setting of Config items with slightly different names to the ones automatically set by getAllAttributes
//
// Revision 1.46  2005/11/02 15:36:03  gcrone
// Set channelId instead of Id for RobinDataChannel
//
// Revision 1.45  2005/11/02 14:20:48  gcrone
// Bug fix, missed index argument on setting Id for RobinDataChannel.
//
// Revision 1.44  2005/11/02 08:41:33  gcrone
// Don't get obsolete L2RequestAll attribute for EmulatedTriggerIn (it's
// been removed from the schema).
//
// Revision 1.43  2005/10/31 16:51:56  gcrone
// Added support for RobinDataDrivenTriggerIn.
//
// Revision 1.42  2005/09/21 13:19:40  gcrone
// Apply Reiner's patch.
//
// Revision 1.41  2005/09/12 14:20:11  gcrone
// Use get_XXX version of DAL generated getters and new Configuration constructor
//
// Revision 1.40  2005/08/03 18:29:24  gcrone
// Set beamType to 0 (not "Dummy") and runType to "test" if no RunParams
// available from IS.
//
// Revision 1.39  2005/08/01 11:50:14  gcrone
// Remove dependence on test for specific DC applications before configuring DC message passing (just do it if we have a DC trigger and or output). Also, remove reference to obsolete RODEmulationTrigger
//
// Revision 1.38  2005/07/27 15:56:46  gcrone
// Remove MessagePassingMemoryPool from RobinReadoutModule and treat
// RobinDataChannel memory configuration in the standard way.
// Also, remove translation of OutputDelay to outputDelay and SamplingGap
// to samplingGap from EmulatedDataOut.
//
// Revision 1.37  2005/07/22 12:40:33  gcrone
// Added processing of MessageLossEmulation relationship for DcTriggerIn.
//
// Revision 1.36  2005/07/20 16:41:26  gcrone
// Get "expert" parameters for a RobinDataChannel as well as
// RobinReadoutModule.
// Use shorter names for ROBIN memory parameters.
// Get MessagePassingMemoryPool attributes for RobinReadoutModule.
//
// Revision 1.35  2005/07/19 14:59:52  gcrone
// Special treatment for RobinDataChannel.
//
// Revision 1.34  2005/07/15 16:13:40  gcrone
// Return 0 early from update() if we can't find our own config in database.
// Remove redundant TCPDataOut specific stuff.  Check that
// MemoryConfiguration relationship is set for an InputChannel before
// trying to access it.
//
// Revision 1.33  2005/07/13 16:19:09  gcrone
// Change handling of multiple data files for PreloadedReadoutModule.
//
// Revision 1.32  2005/07/08 11:22:06  gcrone
// Remove reference to ROBIN class.
//
// Revision 1.31  2005/07/06 18:54:29  gcrone
// Added support for new RobinReadoutModule.  Set rosId for RCD as well
// as ROS.  Get all attributes for ReadoutOutput like we do for
// ReadoutModule and ReadoutTrigger. Allow for ReadoutOutput that
// inherits from more than one parent class.  Issue warning if
// EventStorage relationship not set for EmulatedDataOut instead of
// segfaulting.
//
// Revision 1.30  2005/07/05 16:37:12  gcrone
// Fix rosId for event format 3.0
//
// Revision 1.29  2005/05/17 14:15:07  gcrone
//  Make dummy run number counter a static member so run numbers get
// incremented over multiple incarnations.
//  Remove stuff related to TDAQ_DB environment variables.
//  Remove references to SSPCIReadoutModule and
// EthSequentialReadoutModule.
//
// Revision 1.28  2005/05/17 08:12:41  gcrone
//  Removed error messages when Module/Trigger/Output are unknown since we
// now support arbitrary user plugins through getAllAttributes().
//  Also, increment dummy run number when we can't get run params from IS
// (running with -i).
//
// Revision 1.27  2005/04/28 08:01:07  gcrone
// Removed references to EthReadoutModule and MonDataOut
//
// Revision 1.26  2005/04/26 10:04:08  gcrone
// Add MonitoringScalingFactor to core parameters
//
// Revision 1.25  2005/04/19 16:28:41  gcrone
// Use dfout instead of errorCatcher.
// Set TriggerIn to NullTriggerIn if none specified in database.
// Set DataOut to EmulatedDataOut with output switched off in no DataOut
// specified in database.
// Call getAllAttributes for trigger.
// In getAllAttributes, check for multi value attributes and get as vectors.
//
// Revision 1.24  2005/03/31 19:16:16  gcrone
// BUG Fix: Move setting of DataOut information in coreConfig out of loop
// over InputChannels!!
//
// Revision 1.23  2005/03/17 17:01:13  gcrone
// Use float for values from database declared to be double.  I'm sure no
// configuration values really require double precision.
//
// Revision 1.22  2005/03/11 09:57:11  gcrone
// Make sure we handle all currently known types in getAllAttributes and
// give warning if we get a type we don't know.
//
// Revision 1.21  2005/03/09 16:15:49  gcrone
// Check that all Modules have the same setting for InputFragmentType.
//
// Revision 1.20  2005/03/08 18:11:53  gcrone
// Get all ReadoutModule attributes from Configuration database and set in Config object
//
// Revision 1.19  2005/03/08 08:56:53  gorini
// Moved common module parsing out of the getModule virtual method
//
// Revision 1.18  2005/02/25 21:54:34  gcrone
// Spot the case where an InputChannel doesn't have a BelongsTo
// relationship and report an error rather than dying mysteriously.
//
// Revision 1.17  2005/02/17 17:14:26  sgameiro
// Added EthSequentialReadoutModule
//
// Revision 1.16  2005/02/10 13:59:43  gcrone
// Fix spelling mistake in error message.
//
// Revision 1.15  2005/02/10 13:02:48  gcrone
// Add a couple of checks for obvious errors and report them rather than
// letting null pointer cause segfault.
//
// Revision 1.14  2005/02/08 18:20:56  gcrone
// Set pointers to configuration database for TriggerIn and DataOut in
// getIOManager in case getTriggerIn or getDataOut are overridden by
// subclass.
//

#include "ROSApplication/ApplicationException.h"
#include "ROSApplication/ROSDBConfig.h"
#include "RunControl/Common/OnlineServices.h"

#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <sys/types.h>


// Include files for the database
#include "config/ConfigObject.h"
#include "config/Configuration.h"
#include "config/Schema.h"

//Message Passing stuff
#include "dal/util.h"

//dal headers
#include "dal/Partition.h"
#include "dal/DataFlowParameters.h"
#include "dal/Detector.h"
#include "dal/util.h" //for substitute_variables
#include "dal/Variable.h"
#include "dal/DBConnection.h"
#include "dal/ResourceBase.h"                      // for ResourceBase, Reso...
#include "dal/ResourceSet.h"                       // for ResourceSet

//DFdal headers
#include "DFdal/DataFile.h"
#include "DFdal/DFParameters.h"

#include "DFdal/MessageLossEmulation.h"
#include "DFdal/ReadoutApplication.h"
#include "DFdal/ReadoutConfiguration.h"
#include "DFdal/ROS.h"
#include "DFdal/InputChannel.h"
#include "DFdal/ReadoutModule.h"
#include "DFdal/SequentialInputModule.h"
#include "DFdal/TriggerGenerator.h"                // for TriggerGenerator

#include "DFdal/MemoryPool.h"
#include "DFdal/ReadoutTrigger.h"
#include "DFdal/DataDrivenTriggerIn.h"
#include "DFdal/RobinDataDrivenTriggerIn.h"
#include "DFdal/ReadoutOutput.h"
#include "DFdal/FileDataOut.h"
#include "DFdal/EventStorage.h"
#include "DFdal/ScheduledMonitoringAction.h"
#include "DFdal/SFODBConnection.h"

#include "DFdal/ReadoutPluginConfigurationBase.h"
#include "DFdal/ReadoutPluginConfigurationMUX.h"
#include "DFdal/ReadoutPluginConfiguration.h"

#include "SFOTZ/SFOTZThread.h"

// IS
#include "is/infoT.h"
#include "is/infoiterator.h"
#include "is/exceptions.h"                         // for Exception
#include "is/infodictionary.h"                     // for ISInfoDictionary

#include "rc/RunParams.h"                          // for RunParams
#include "ers/ers.h"                               // for fatal, warning, error


#include "ROSUtilities/ROSErrorReporting.h"
#include "DFSubSystemItem/ConfigException.h"

#include "DFSubSystemItem/Config.h"                // for Config, Config::T_...
#include "DFSubSystemItem/SSIConfiguration.h"      // for SSIConfiguration
#include "DFThreads/DFCountedPointer.h"            // for DFCountedPointer

using namespace ROS;

int ROSDBConfig::s_dummyRunNumber=1000;

ROSDBConfig::ROSDBConfig(char* rosName) : m_rosName(rosName),
                                          m_confDB(0),
                                          m_dbPartition(0),
                                          m_dbDetectorId(0),
                                          m_rosId(0) { 

  // For the Run parameters stored in IS
  m_partitionName=getenv("TDAQ_PARTITION");
  m_IPCPartition = new IPCPartition(m_partitionName);
  if(!m_IPCPartition) {
    CREATE_ROS_EXCEPTION(ex, ApplicationException, IPC_CREATE_FAIL, "");
    ers::fatal(ex);
  }
  m_ISDict = new ISInfoDictionary(*m_IPCPartition);
  if(!m_ISDict) {
    CREATE_ROS_EXCEPTION(ex, ApplicationException, ISDICTIONARY_CREATE_FAIL, "");
    ers::fatal(ex);
  }

  // Process global options string
  size_t index=0;
  while ((s_globalOptions.size() > index) && (index != std::string::npos)) {
     size_t eqPos=s_globalOptions.find("=",index);
     m_globalKeys.push_back(std::string(s_globalOptions,index,eqPos));
     if (eqPos != std::string::npos) {
        index=s_globalOptions.find(",",eqPos+1);
        size_t endPos=index;
        if (index != std::string::npos) {
           index++;
           endPos-=eqPos+1;
        }
        m_globalValues.push_back(std::string(s_globalOptions,eqPos+1,endPos));
     }
     else {
        // No value given, set it to 1 or true
        m_globalValues.push_back("1");
        index=std::string::npos;
     }
  }
}

ROSDBConfig::~ROSDBConfig() { 
  if(m_ISDict) {
    delete m_ISDict;
    m_ISDict = 0;
  }
  if(m_IPCPartition) {
    delete m_IPCPartition;
    m_IPCPartition = 0;
  }
}

SSIConfiguration * ROSDBConfig::update() {
  // Load the DB
  loadDB();
  if(!m_confDB) {
     return 0;
  }

  daq::rc::OnlineServices& os=daq::rc::OnlineServices::instance();
  if(!(m_dbPartition = &os.getPartition())) {

    CREATE_ROS_EXCEPTION(ex, ApplicationException, NO_PARTITION, "");
    ers::fatal(ex);
    return 0;
  }


  // get application configuration
  std::cout << "ROSDBConfig::update getting config for ReadoutApplication " << m_rosName << std::endl;
  const daq::df::ReadoutApplication* dbApp= m_confDB->get<daq::df::ReadoutApplication>(m_rosName);
  if (dbApp==0) {
     CREATE_ROS_EXCEPTION(ex, ApplicationException, NOT_IN_DB, m_rosName);
     ers::fatal(ex);
     return 0;
  }

  return getIOManager(dbApp);
}

Configuration* ROSDBConfig::getConfiguration() {
   return m_confDB;
}

const daq::core::Partition* ROSDBConfig::getPartition() {
   return m_dbPartition;
}

SSIConfiguration * ROSDBConfig::updateRun() {
  //Build new information
  DFCountedPointer<Config> runParams = Config::New();
  runParams->set("appName", m_rosName);

  try {
     RunParams ISrunParams; 
     m_ISDict->getValue("RunParams.RunParams", ISrunParams);
    runParams->set("recording", ISrunParams.recording_enabled);
    runParams->set("runNumber", ISrunParams.run_number);
    runParams->set("maxNumberOfEvents", ISrunParams.max_events);
    runParams->set("triggerType", ISrunParams.trigger_type);
    runParams->set("detectorMask", ISrunParams.det_mask);
    runParams->set("beamType", ISrunParams.beam_type);
    runParams->set("beamEnergy", ISrunParams.beam_energy);
    runParams->set("fileTag", ISrunParams.filename_tag);

    runParams->set("T0ProjectTag", ISrunParams.T0_project_tag);

    runParams->set("runType", ISrunParams.run_type);
    int item=0;
    ISInfoT< std::vector<std::string> > isMetaData;
    try {
       m_ISDict->getValue("RunParams.UserMetaData", isMetaData);
       std::vector<std::string> metaData=isMetaData.getValue();
       item=metaData.size();
       runParams->setVector<std::string>("UserMetaData", metaData);

       if (item!=0) {
          runParams->set("UserDataPresent", true);
       }
       else {
          runParams->set("UserDataPresent", false);
       }
    }
    catch (daq::is::Exception& userDataException) {
       runParams->set("UserDataPresent", false);
    }
  }
  catch (daq::is::Exception& runParamsException) {
     CREATE_ROS_EXCEPTION(ex, ApplicationException, NOT_IN_IS,  "runParams");
     ers::warning(ex);
     runParams->set("recording", true);
     runParams->set("runNumber", s_dummyRunNumber++);
     runParams->set("maxNumberOfEvents", 0);
     runParams->set("triggerType", 0);
     runParams->set("detectorMask", "");
     runParams->set("beamType", 0);
     runParams->set("beamEnergy", 0);
     runParams->set("fileTag", "DAQ");
     runParams->set("T0ProjectTag", "");
     runParams->set("runType", "test");
     runParams->set("UserDataPresent", false);
  }

  Config::T_ConfigContainer runParamsConfig;
  runParamsConfig.push_back(runParams);
  SSIConfiguration * ssiRunParams = new SSIConfiguration();
  ssiRunParams->setConfig("RunParameters", runParamsConfig); 

  return ssiRunParams;
}

// Auxiliary methods
//------------------------------------------------------------------------------
void ROSDBConfig::loadDB() {
    // this is the config/Configuration, not the daq::df::Configuration!!
    if (!m_confDB) { // First loading of DB
      daq::rc::OnlineServices& os=daq::rc::OnlineServices::instance();
      m_confDB=&os.getConfiguration();
    }

    if(!m_confDB->loaded()) {
       CREATE_ROS_EXCEPTION(ex, ApplicationException, LOAD_FAILURE, "");
       ers::fatal(ex);
       delete m_confDB;
       m_confDB = 0;
    }

}

//-------------------------------------------------------------

void ROSDBConfig::getMemConfig(const daq::df::MemoryPool * pool, DFCountedPointer<Config> pairs) {
  if(pool) {
    pairs->set("memoryPoolNumPages", (int) (pool->get_NumberOfPages()));
    pairs->set("memoryPoolPageSize", (int) (pool->get_PageSize()));
    if(pool->get_Type() == "MALLOC") {
      pairs->set("poolType", 1);
    }
    else if (pool->get_Type() == "CMEM"){
      pairs->set("poolType", 2);
    }
    else {
      pairs->set("poolType", 3); // means CMEM_BPA
    }
  }
}

//---------------------------------------------------------------


void ROSDBConfig::getAllAttributes(const std::string itemClass, const std::string itemId,
                                   DFCountedPointer<Config>pairs, int index)
{
   ConfigObject module;
   m_confDB->get(itemClass, itemId, module);
   std::vector<std::string> attribute;
   auto classConfig=m_confDB->get_class_info(module.class_name());
   for (auto iter: classConfig.p_attributes) {
      string attrName=iter.p_name;
      auto attrType=iter.p_type;
      bool attrMulti=iter.p_is_multi_value;
      //            cerr <<  "\tGot attribute " << attrName << " type " << attrType << " multi " << attrMulti << " from DB\n";

      if ((attrType==daq::config::u32_type) || (attrType==daq::config::u16_type) || (attrType==daq::config::u8_type)) {
         if (attrMulti) {
            vector <uint32_t> values;
            module.get(attrName, values);
            pairs->setVector(attrName, values);
         }
         else {
            uint32_t value;
            module.get(attrName, value);
            pairs->set(attrName, value, index);
         }
      }
      else if (attrType==daq::config::u64_type) {
         if (attrMulti) {
            vector <uint64_t> values;
            module.get(attrName, values);
            pairs->setVector(attrName, values);
         }
         else {
            uint64_t value;
            module.get(attrName, value);
            pairs->set(attrName, value, index);
         }
      }
      else if (attrType==daq::config::s64_type) {
         if (attrMulti) {
            vector <int64_t> values;
            module.get(attrName, values);
            pairs->setVector(attrName, values);
         }
         else {
            int64_t value;
            module.get(attrName, value);
            pairs->set(attrName, value, index);
         }
      }
      else if (attrType==daq::config::s32_type) {
         if (attrMulti) {
            vector <int32_t> values;
            module.get(attrName, values);
            pairs->setVector(attrName, values);
         }
         else {
            int32_t value;
            module.get(attrName, value);
            pairs->set(attrName, value, index);
         }
      }
      else if (attrType==daq::config::s16_type) {
         if (attrMulti) {
            vector <uint16_t> values;
            module.get(attrName, values);
            pairs->setVector(attrName, values);
         }
         else {
            int16_t value;
            module.get(attrName, value);
            pairs->set(attrName, value, index);
         }
      }
      else if (attrType==daq::config::s8_type) {
         if (attrMulti) {
            vector <int8_t> values;
            module.get(attrName, values);
            pairs->setVector(attrName, values);
         }
         else {
            int8_t value;
            module.get(attrName, value);
            pairs->set(attrName, value, index);
         }
      }
      else if (attrType==daq::config::float_type) {
         if (attrMulti) {
            vector <float> values;
            module.get(attrName, values);
            pairs->setVector(attrName, values);
         }
         else {
            float value;
            module.get(attrName, value);
            pairs->set(attrName, value, index);
         }
      }
      else if (attrType==daq::config::double_type) {
         if (attrMulti) {
            vector <double> values;
            module.get(attrName, values);
            pairs->setVector(attrName, values);
         }
         else {
            double value;
            module.get(attrName, value);
            pairs->set(attrName, value, index);
         }
      }
      else if (attrType==daq::config::bool_type) {
         if (attrMulti) {
            vector <bool> values;
            module.get(attrName, values);
            pairs->setVector(attrName, values);
         }
         else {
            bool value;
            module.get(attrName, value);
            pairs->set(attrName, value, index);
         }
      }
      else if ((attrType==daq::config::string_type) || (attrType==daq::config::enum_type) ||
               (attrType==daq::config::date_type) || (attrType==daq::config::time_type)) {
         if (attrMulti) {
            vector <std::string> values;
            module.get(attrName, values);
            pairs->setVector(attrName, values);
         }
         else {
            std::string value;
            module.get(attrName, value);
            pairs->set(attrName, value, index);
         }
      }
      else {
         CREATE_ROS_EXCEPTION(ex, ApplicationException, BAD_ATTRIBUTE_TYPE, attrType << " for " << attrName);
         ers::warning(ex);
      }
   }

   for (auto iter: classConfig.p_relationships) {
      string relName=iter.p_name;
      string relType=iter.p_type;

      if (relName=="Configuration") {
         std::cout << "Getting Configuration relationship for " << module.UID() << ", type=" << relType << std::endl;

         ConfigObject relObj;
         module.get(relName, relObj);
         if (!relObj.is_null()) {
            bool configFound=true;
            std::string configUid=relObj.UID();
            std::string confClass=relObj.class_name();
            if (confClass.find("MUX")!=std::string::npos) {
               configFound=false;
               ConfigObject selector;
               relObj.get("Selector",selector);
               if (!selector.is_null()) {
                  std::cout << "Selector variable is " << selector.UID() << std::endl;
                  std::string configName;
                  selector.get("Value",configName);
                  std::cout << "Searching " << confClass << " " << configUid << " for confgiguration " << configName << "\n";

                  std::vector<ConfigObject> configs;
                  relObj.get("Configurations",configs);
                  for (std::vector<ConfigObject>::iterator confIter=configs.begin() ;
                       confIter!=configs.end() ; confIter++) {
                     std::string confType;
                     (*confIter).get("ConfigurationType",confType);
                     if (confType==configName) {
                        confClass=(*confIter).class_name();
                        configUid=(*confIter).UID();
                        configFound=true;
                     }
                  }
               }
            }

            if (configFound) {
               std::cout << "Processing Configuration " << configUid << " (type " << confClass << ") for " << module.UID() << std::endl;
               getAllAttributes(confClass, configUid, pairs, index);
            }
         }
      }
      else if (relName=="ExpertParameters") {
         std::vector<ConfigObject> expertPars;
         module.get(relName,expertPars);
         for (std::vector<ConfigObject>::iterator confIter=expertPars.begin() ;
              confIter!=expertPars.end() ; confIter++) {
            std::string name;
            (*confIter).get("Name",name);
            uint32_t value;
            (*confIter).get("Value",value);
            pairs->set(name,value,index);
         }
      }
   }
}

ConfigObject ROSDBConfig::getSelectedConfig(ConfigObject& object) {

   ConfigObject selector;
   object.get("Selector",selector);
   if (!selector.is_null()) {
      std::cout << "Selector variable is " << selector.UID() << std::endl;
      std::string configName;
      selector.get("Value",configName);
      std::cout << "Searching " << object.class_name() << " " << object.UID() << " for configuration " << configName << "\n";

      std::vector<ConfigObject> configs;
      object.get("Configurations",configs);
      for (std::vector<ConfigObject>::iterator confIter=configs.begin() ;
           confIter!=configs.end() ; confIter++) {
         std::string confType;
         (*confIter).get("ConfigurationType",confType);
         if (confType==configName) {
            return (*confIter);
         }
      }
   }
   return 0;
}



std::vector<const daq::core::ResourceBase*> ROSDBConfig::getChannels(const daq::core::ResourceSet* module) {
  std::vector<const daq::core::ResourceBase*> channels;

  if(module) {
    std::vector<const daq::core::ResourceBase *> resources = module->get_Contains();

    daq::core::ResourceBaseIterator it;
  
    for(it = resources.begin(); it != resources.end(); ++it) {
      if(const daq::core::ResourceSet* rset = m_confDB->cast<daq::core::ResourceSet, daq::core::ResourceBase> ((*it))) {
        std::vector<const daq::core::ResourceBase *> rbs = rset->get_Contains();
        daq::core::ResourceBaseIterator ri;
  
        for(ri = rbs.begin(); ri != rbs.end(); ++ri) {
          if(m_confDB->cast<daq::df::InputChannel, daq::core::ResourceBase> ((*ri))) {
            channels.push_back ((*ri));
          }
          // else it could be the other end of the Link so don't worry about it
        }
      }
      else {
         // Accept anything else here, we'll check for errors higher up
         channels.push_back ((*it));
      }
    }
  }

  return channels;
}

SSIConfiguration * ROSDBConfig::getIOManager(const daq::df::ReadoutApplication* dbApp ) {

  m_rosId = dbApp->get_Id();

  if (dbApp->disabled(*m_dbPartition)) {
     CREATE_ROS_EXCEPTION(ex, ApplicationException, BAD_CONFIGURATION,
                            ": ReadoutApplication " << m_rosName
                            << " is disabled in the database");
     ers::error(ex);
     return 0;
  }
    // Get The Detector Id
  if(dbApp->get_Detector()) {
    m_dbDetectorId = dbApp->get_Detector()->get_LogicalId();
  }

  // Core IOManager information
  Config::T_ConfigContainer coreConfig = getCore(dbApp);  
    
  // Add some informations for Core!
  // Peripherals
  bool dataDriven=false;
  bool descriptorTrigger=false;
  int triggerQueueSize=0;
  int triggerQueueUnblockOffset=0;
  bool triggerSingleFragment=false;
  int triggerCount=0;
  unsigned int robinProbeInterval=0;
  Config::T_ConfigContainer trgConfig;

  std::vector<const daq::df::ReadoutTrigger* >::const_iterator trigStartIter;
  std::vector<const daq::df::ReadoutTrigger* >::const_iterator trigEndIter;

  const daq::df::ReadoutPluginConfiguration* plugins=0;
  const daq::df::ReadoutPluginConfigurationBase* pluginConf=const_cast<daq::df::ReadoutPluginConfigurationBase*>(dbApp->get_PluginConfiguration());
  if (pluginConf!=0) {
     std::cout << "PluginConfiguration is set\n";
     if (pluginConf->class_name()=="ReadoutPluginConfigurationMUX") {
        const daq::df::ReadoutPluginConfigurationMUX* mux=m_confDB->cast<daq::df::ReadoutPluginConfigurationMUX>(pluginConf);
        const daq::core::Variable* selector=mux->get_Selector();
        std::string selection=selector->get_value();
        std::cout << "PluginConfiguration is a MUX, looking for selection <" << selection << ">\n";

        std::vector<const daq::df::ReadoutPluginConfiguration*> configs=mux->get_Configurations();
        for (std::vector<const daq::df::ReadoutPluginConfiguration*>::const_iterator confIter=configs.begin(); confIter!=configs.end(); confIter++) {
           if ((*confIter)->get_ConfigurationType()==selection) {
              std::cout << "Found PluginConfiguration " << selection << "\n";
              plugins=(*confIter);
              break;
           }
        }
        if (plugins==0) {
           std::cout << "Failed to find PluginConfiguration " << selection << "\n";
           CREATE_ROS_EXCEPTION(ex, ApplicationException, BAD_CONFIGURATION,
                                ": ReadoutApplication " << m_rosName
                                << " ReadoutPluginConfigurationMUX '" << mux->UID()
                                << "' does not have a configuration with ConfigurationType " << selection);
           ers::error(ex);
           return 0;
        }
     }
     else {
        std::cout << "PluginConfiguration is a simple object\n";
        plugins=m_confDB->cast<daq::df::ReadoutPluginConfiguration>(pluginConf);
     }
     trigStartIter=plugins->get_TriggerIn().begin();
     trigEndIter=plugins->get_TriggerIn().end();
  }
  else {
     trigStartIter=dbApp->get_Trigger().begin();
     trigEndIter=dbApp->get_Trigger().end();
  }

  bool monitoringOutDefined=false;
  bool debugOutDefined=false;

  for(std::vector<const daq::df::ReadoutTrigger * >::const_iterator iter =trigStartIter;
      iter !=trigEndIter; ++iter) {
     daq::df::ReadoutTrigger * usedTrigger =const_cast<daq::df::ReadoutTrigger*>(*iter);

     if (usedTrigger->class_name() == "NPTriggerIn") {
        descriptorTrigger=true;
        monitoringOutDefined=true;
        debugOutDefined=true;
     }
     else if (usedTrigger->class_name() == "DataDrivenTriggerIn") {
        dataDriven=true;
        const daq::df::DataDrivenTriggerIn* ddTi = 
           m_confDB->cast<daq::df::DataDrivenTriggerIn, daq::df::ReadoutTrigger>(usedTrigger);
        triggerQueueSize=ddTi->get_QueueSize();
        triggerQueueUnblockOffset=ddTi->get_QueueUnblockOffset();
        triggerSingleFragment=ddTi->get_SingleFragmentMode();
     }
     else if (const daq::df::RobinDataDrivenTriggerIn* ddTi = 
           m_confDB->cast<daq::df::RobinDataDrivenTriggerIn, daq::df::ReadoutTrigger>(usedTrigger)) {
        dataDriven=true;
        triggerQueueSize=ddTi->get_QueueSize();
        triggerQueueUnblockOffset=ddTi->get_QueueUnblockOffset();

        robinProbeInterval=ddTi->get_RobinProbeInterval();
     }

     DFCountedPointer<Config> trgPairs = getTrigger(usedTrigger);
     for (unsigned int opt=0; opt < m_globalKeys.size(); opt++) {
        trgPairs->set(m_globalKeys[opt],m_globalValues[opt]);
     }

     //set pointer to database here in case derived class overrides getTrigger method
     trgPairs->setPointer("configurationDB", m_confDB);
     trgPairs->setPointer("partitionPointer", m_dbPartition);
     trgPairs->set("partition", m_partitionName);
     trgPairs->set("appName", m_rosName);
     trgPairs->set("UID", usedTrigger->UID());
     getAllAttributes("ReadoutTrigger", usedTrigger->UID(), trgPairs);

     //    cout<<"+++++++++   trgPairs  " << usedTrigger->UID() << " +++++++++\n";
     //    trgPairs->dump();

     trgConfig.push_back(trgPairs);

     coreConfig[0]->set("triggerInType", usedTrigger->class_name(), triggerCount++);
  }
  coreConfig[0]->set("numberOfTriggerIns", triggerCount);


  std::vector<const daq::df::ReadoutOutput*> usedOutputs;
  int outputCount=0;
  Config::T_ConfigContainer outConfig;
  const daq::df::ReadoutOutput* output=0;
  if (plugins!=0) {
     std::cout << "Getting main output from plugin conf\n";
     output=plugins->get_MainOutput();
  }
  else {
     std::cout << "Getting main output from application conf\n";
     output=dbApp->get_Output();
  }
  if(output) {
     usedOutputs.push_back(output);
     coreConfig[0]->set("dataOutType", "MAIN", outputCount);
     coreConfig[0]->set("dataOutClass", output->class_name(), outputCount);
     outputCount++;
  }

  if (plugins!=0) {
     output=plugins->get_DebugOutput();
  }
  else {
     output=dbApp->get_DebugOutput();
  }
  if(output) {
     debugOutDefined=true;
     usedOutputs.push_back(output);
     coreConfig[0]->set("dataOutType", "DEBUGGING", outputCount);
     coreConfig[0]->set("dataOutClass", output->class_name(), outputCount);
     outputCount++;
  }

  if (plugins!=0) {
     output=plugins->get_MonitoringOutput();
  }
  else {
     output=dbApp->get_MonitoringOutput();
  }
  if(output) {
     monitoringOutDefined=true;
     usedOutputs.push_back(output);
     coreConfig[0]->set("dataOutType", "SAMPLED", outputCount);
     coreConfig[0]->set("dataOutClass", output->class_name(), outputCount);
     outputCount++;
  }

  // Get Output config
  outConfig  = getOutput(usedOutputs);

  if (!debugOutDefined) {
     coreConfig[0]->set("dataOutType", "DEBUGGING", outputCount);
     coreConfig[0]->set("dataOutClass", "EMonDataOut", outputCount);
     DFCountedPointer<Config> dbgConfig=Config::New();
     dbgConfig->set("partition", m_partitionName);
     dbgConfig->set("appName", m_rosName);
     dbgConfig->set("key", "ReadoutApplicationDebug");
     dbgConfig->set("value", m_rosName);
     dbgConfig->set("UID", "FAKE_DEBUG_OUTPUT");
     outConfig.push_back(dbgConfig);
     outputCount++;
  }
  if (!monitoringOutDefined) {
     coreConfig[0]->set("dataOutType", "SAMPLED", outputCount);
     coreConfig[0]->set("dataOutClass", "EMonDataOut", outputCount);
     DFCountedPointer<Config> monConfig=Config::New();
     monConfig->set("partition", m_partitionName);
     monConfig->set("appName", m_rosName);
     monConfig->set("UID", "FAKE_MONITORING_OUTPUT");
     outConfig.push_back(monConfig);
     outputCount++;
  }

  int index=0;
  for (std::vector<const daq::df::ReadoutOutput*>::iterator outIter=usedOutputs.begin();
       outIter!=usedOutputs.end(); outIter++) {

     string roOutputUID=(*outIter)->UID();

     //set pointer to database here in case derived class overrides getOutput method
     outConfig[index]->setPointer("configurationDB", m_confDB);
     outConfig[index]->set("appName", m_rosName);
     outConfig[index]->set("UID", roOutputUID);
     outConfig[index]->set("partition", m_partitionName);

     //     std::cout << "+++++++++   outConfig[" << index << "]   +++++++++\n";
     //     outConfig[index]->dump();

     index++;
  }
  coreConfig[0]->set("numberOfDataOuts", outputCount);

  // Get the Modules and InputChannels
  Config::T_ConfigContainer fmConfig;
  map<std::string,  const daq::df::ReadoutModule*> modules;
  map<std::string,  DFCountedPointer<Config> > fmPairs;


  // Loop over Devices
  int lastFragmentType=-1;
  int moduleCount=0;
  for (std::vector<const daq::core::ResourceBase*>::const_iterator device = dbApp->get_Contains().begin(); 
       device != dbApp->get_Contains().end(); device++) {
    string mId((*device)->UID());

    const daq::df::ReadoutModule*  roModule=m_confDB->cast<daq::df::ReadoutModule, daq::core::ResourceBase> (*device);
    if (roModule==0) {
       CREATE_ROS_EXCEPTION(ex, ApplicationException, BAD_CONFIGURATION,
                            ": ReadoutApplication " << m_rosName
                            << " Contains relationship to something (" << (*device)->class_name() <<
                            ") that is not a ReadoutModule");
       ers::fatal(ex);
       continue;
    }
    else if (!(*device)->disabled(*m_dbPartition)) {
       coreConfig[0]->set("readoutModuleType", roModule->class_name(), moduleCount++);
       modules[mId] = roModule;
       fmPairs[mId] = Config::New();

       for (unsigned int opt=0; opt < m_globalKeys.size(); opt++) {
          fmPairs[mId]->set(m_globalKeys[opt],m_globalValues[opt]);
       }

       fmPairs[mId]->set("ReadoutModuleClass", roModule->class_name());

       // Set info about the actual configuration DB so that Modules can 
       // "do their own thing"
       fmPairs[mId]->setPointer("configurationDB", m_confDB);
       fmPairs[mId]->setPointer("partitionPointer", m_dbPartition);
       fmPairs[mId]->set("appName", m_rosName);
       fmPairs[mId]->set("UID", mId);

       // Values from DataDrivenTriggerIn used by sequential readout modules
       fmPairs[mId]->set("triggerQueue",dataDriven);
       fmPairs[mId]->set("triggerQueueSize",triggerQueueSize);
       fmPairs[mId]->set("triggerQueueUnblockOffset",triggerQueueUnblockOffset);
       fmPairs[mId]->set("SingleFragmentMode",triggerSingleFragment);

       // From RobinDataDrivenTriggerIn, used by RobinReadoutModule
       fmPairs[mId]->set("RobinProbeInterval", robinProbeInterval);

       // Set values common to ALL modules
       fmPairs[mId]->set("numberOfChannels", 0);      

       int fragmentType;

       const daq::df::SequentialInputModule*  seqModule=m_confDB->cast<daq::df::SequentialInputModule, daq::core::ResourceBase> (*device);
       if (seqModule != 0) {
          if (seqModule->get_InputFragmentType() == "RODFragment") {
             fragmentType=0;
          }
          else {
             fragmentType=1;
          }

          if (lastFragmentType == -1) {
             lastFragmentType=fragmentType;
          }
          else if (fragmentType != lastFragmentType) {
             CREATE_ROS_EXCEPTION(ex, ApplicationException, BAD_CONFIGURATION, " Modules have different InputFragmentType");
             ers::fatal(ex);
          }
          fmPairs[mId]->set("inputFragmentType", fragmentType);
       }


       // Get other attributes of this module
       getAllAttributes ("ReadoutModule", mId, fmPairs[mId]);

       getModule(modules[mId], fmPairs[mId]);


       int channelNumber=0;

       const daq::core::ResourceSet*  resources=m_confDB->cast<daq::core::ResourceSet, daq::core::ResourceBase> (*device);
       std::vector<const daq::core::ResourceBase*> channel = getChannels(resources);
       for(std::vector<const daq::core::ResourceBase*>::const_iterator channelIter = channel.begin(); 
           channelIter != channel.end(); channelIter++) {

          if (!(*channelIter)->disabled(*m_dbPartition)) {
             const daq::df::InputChannel* channelPtr=m_confDB->cast<daq::df::InputChannel, daq::core::ResourceBase> (*channelIter);
             if (channelPtr!=0) {
                appendToModule(channelNumber, channelPtr, fmPairs[mId]);
             }
             else {
                getAllAttributes("ResourceBase", (*channelIter)->UID(), fmPairs[mId], channelNumber);
             }
             fmPairs[mId]->set("ChannelClass", (*channelIter)->class_name(), channelNumber);
             channelNumber++;
          }
          else {
             std::cout << "Resource " << (*channelIter)->UID() << " is disabled\n";
          }
       }
       fmPairs[mId]->set("numberOfChannels", channelNumber);      

       //       cout<<"+++++++++   fmPairs " << mId << "   " << roModule->UID() << " +++++++++\n";
       //       fmPairs[mId]->dump();
       fmConfig.push_back(fmPairs[mId]);
    }
    else {
       cout << "ReadoutModule " << roModule->UID() << " is disabled\n";
    }
  }
  coreConfig[0]->set("numberOfReadoutModules", moduleCount);

  if (dataDriven && (coreConfig[0]->getInt("NumberOfRequestHandlers") != 1)) {
     coreConfig[0]->set("NumberOfRequestHandlers", 1);
     CREATE_ROS_EXCEPTION(ex, ApplicationException, OVERRIDE,
                          " forcing numberOfRequestHandlers to 1 for data driven trigger");
     ers::warning(ex);
  }
  else if (descriptorTrigger) {
     coreConfig[0]->set("NumberOfRequestHandlers", 0);
  }
  // Prepare SSIConfiguration
  SSIConfiguration* ssiConf = new SSIConfiguration();
  ssiConf->setConfig("TriggerIn", trgConfig);
  ssiConf->setConfig("DataOut", outConfig);
  ssiConf->setConfig("ReadoutModule", fmConfig);
  ssiConf->setConfig("Core", coreConfig);

  return ssiConf;
}

//---------------------------------------------------------------

DFCountedPointer<Config> ROSDBConfig::getTrigger(const daq::df::ReadoutTrigger * trg) {

  DFCountedPointer<Config> trgPairs = Config::New();

  // deleteGrouping is there for all triggers
  trgPairs->set("deleteGrouping", trg->get_DeleteGrouping());


  // Get the right TriggerIn  
  if (const daq::df::TriggerGenerator * trigGen = 
      m_confDB->cast<daq::df::TriggerGenerator, daq::df::ReadoutTrigger>(trg)) {
    trgPairs->set("destinationNodeId", 12289); 
    // Get RoI distribution
    trgPairs->set("maxRolsInRoI",      trigGen->get_ROIDistribution().size());
    for(unsigned int n = 0; n < trigGen->get_ROIDistribution().size(); n++) {
      trgPairs->set("probRols", trigGen->get_ROIDistribution()[n], n+1);
    } 
  }

  trgPairs->set("SubDetectorId", m_dbDetectorId);

  //  cout << "====================== TriggerIn Config =============\n";
  //  trgPairs->dump();
  //  cout << "=====================================================\n";

  return trgPairs;
}

Config::T_ConfigContainer ROSDBConfig::getOutput(std::vector<const daq::df::ReadoutOutput*> dbOut) {

  Config::T_ConfigContainer outConfig;

  for(std::vector<const daq::df::ReadoutOutput*>::const_iterator pOut = dbOut.begin();
      pOut != dbOut.end(); pOut++) {
    DFCountedPointer<Config> outPairs = Config::New();
    getOutputChannel(*pOut, outPairs);
    getAllAttributes("ReadoutOutput", (*pOut)->UID(), outPairs);
    outConfig.push_back(outPairs);
  }
  return outConfig;
}

void ROSDBConfig::getOutputChannel(const daq::df::ReadoutOutput * pOut, DFCountedPointer<Config> outPairs) {

    if(const daq::df::FileDataOut * fileOut = 
       m_confDB->cast<daq::df::FileDataOut, daq::df::ReadoutOutput>(pOut)) {
       if (fileOut->get_EventStorageConfiguration()!=0) {
          outPairs->set("writingPath", fileOut->get_EventStorageConfiguration()->get_DirectoryToWrite());
          outPairs->set("maxEventsPerFile", fileOut->get_EventStorageConfiguration()->get_MaxEventsPerFile());
          outPairs->set("maxMegaBytesPerFile", fileOut->get_EventStorageConfiguration()->get_MaxMegaBytesPerFile());
       }
       else {
          CREATE_ROS_EXCEPTION(ex, ApplicationException, REL_NOT_IN_DB, " EventStorage relationship for FileDataOut");
          ers::warning(ex);
       }
       const daq::df::SFODBConnection* dbConnection=fileOut->get_TZdbConfiguration();
       if (dbConnection!=0) {
          const daq::core::DBConnection * main = dbConnection->get_MainDBConnection();
          outPairs->set("runTable",dbConnection->get_RunTableName());
          outPairs->set("lbTable",dbConnection->get_LumiBlockTableName());
          outPairs->set("fileTable",dbConnection->get_FileTableName());
          outPairs->set("indexTable",dbConnection->get_IndexTable());
          std::string connString(SFOTZ::SFOTZThread::buildConnectionString(main->get_Server(),
                                                                           main->get_Name(),
                                                                           main->get_Type()));
          outPairs->set("connectionString", connString);
          outPairs->set("username", main->get_User());
          outPairs->set("password", main->get_Password());
       }
    }
}

void ROSDBConfig::appendToModule(int index, const daq::df::InputChannel* channel, DFCountedPointer<Config> fmPairs) {

  if (channel->get_MemoryConfiguration() != 0) {
     fmPairs->set("memoryPoolNumPages", (int) (channel->get_MemoryConfiguration()->get_NumberOfPages()), index);
     fmPairs->set("memoryPoolPageSize", (int) (channel->get_MemoryConfiguration()->get_PageSize()), index);

     int poolType;
     if(channel->get_MemoryConfiguration()->get_Type() == "MALLOC") {
        poolType=1;
     }
     else if (channel->get_MemoryConfiguration()->get_Type() == "CMEM"){
        poolType=2;
     }
     else {
        poolType=3; // means CMEM_BPA
     }
     fmPairs->set("poolType", poolType, index);
  }
  else {
     CREATE_ROS_EXCEPTION(ex, ApplicationException, REL_NOT_IN_DB, " MemoryConfiguration relationship for channel Id "
                        << channel->get_Id());
     ers::warning(ex);
  }

  if (channel->class_name()=="RobinDataChannel") {
     // Now get the rest from a "RobinDataChannel" NOT a simple "InputChannel"
     getAllAttributes("RobinDataChannel", channel->UID(), fmPairs, index);

     unsigned int id=channel->get_Id();
     fmPairs->set("channelId", id, index);

     // Hack to ensure that detector ID part of channel ID is not set
     id &= 0x0000ffff;
     fmPairs->set("ChannelId", id, index);

  }
  else {
     unsigned int channelId=channel->get_Id();
     //     cout<< "Setting ID for "<<channel->class_name()<< " "<<hex<<channelId<< " ==> ";
     string rmClass;
     try {
        rmClass=fmPairs->getString("ReadoutModuleClass");
     }
     catch (ConfigException& error) {
     }
        
     if (rmClass != "PreloadedReadoutModule") {
        // fold in detector ID from ROS
        channelId=(channelId & 0x0000ffff) | (m_dbDetectorId << 16);
     }
     //     cout <<channelId<<dec<<endl;
     fmPairs->set("channelId", channelId, index);
     getAllAttributes("InputChannel", channel->UID(), fmPairs, index);
  }

  if (fmPairs->isKey("PhysAddress", index)) {
     fmPairs->set("ROLPhysicalAddress", fmPairs->getString("PhysAddress", index), index);
  }
  else {
     fmPairs->set("ROLPhysicalAddress", 0, index);
  }

  fmPairs->set("UID", channel->UID(), index);
}


void ROSDBConfig::getModule(const daq::df::ReadoutModule* rom, DFCountedPointer<Config> fmPairs) {
   fmPairs->set("PhysicalAddress", rom->get_PhysAddress());
   fmPairs->set("SubDetectorId", m_dbDetectorId);   // The SubDetectorId comes from the ROS

   if(rom->class_name() == "EmulatedReadoutModule") {
     fmPairs->set("detectorEventType",m_dbDetectorId);
   }
   else if(rom->class_name() == "PreloadedReadoutModule") {
      if(m_dbPartition->get_DataFlowParameters()) {
        if(m_dbPartition->get_DataFlowParameters()->class_name() == "DFParameters") {
	   const std::vector<const daq::df::DataFile*> dFvec = 
              m_confDB->cast<daq::df::DFParameters, daq::core::DataFlowParameters>(m_dbPartition->get_DataFlowParameters())
              ->get_UsesDataFiles();
           if(dFvec.size() > 0) {
              int index=0;
              for(std::vector<const daq::df::DataFile*>::const_iterator dF = 
                     dFvec.begin(); dF != dFvec.end(); dF++) {
                 fmPairs->set("dataFiles", (*dF)->get_FileName(), index++);
              }
           }
        }
      }
   }
}

//------------------------------------------------------------------------------

Config::T_ConfigContainer ROSDBConfig::getCore(const daq::df::ReadoutApplication* dbApplication) {

  DFCountedPointer<Config> corePairs = Config::New();


  corePairs->setPointer("configurationDB", m_confDB);
  corePairs->setPointer("partitionPointer", m_dbPartition);

  corePairs->set("partition", m_partitionName);

  getAllAttributes("ReadoutApplication", dbApplication->UID(), corePairs);

  int commTimeout=corePairs->getInt("ActionTimeout");
  if (commTimeout > 2) {
     commTimeout-=2;
  }
  corePairs->set("communicationTimeout", commTimeout);

  corePairs->set("rosId", (m_dbDetectorId << 16) + m_rosId);

  getMemConfig(dbApplication->get_MemoryConfiguration(), corePairs);

  const daq::df::MemoryPool *pool=dbApplication->get_MonitoringMemoryConfiguration();
  if (pool != 0) {
     corePairs->set("UserCommandPoolPageSize", pool->get_PageSize());
     if(pool->get_Type() == "MALLOC") {
        corePairs->set("UserCommandPoolType", 1);
     }
     else if (pool->get_Type() == "CMEM"){
        corePairs->set("UserCommandPoolType", 2);
     }
     else {
        corePairs->set("UserCommandPoolType", 3); // means CMEM_BPA
     }
  }
  else {
     corePairs->set("UserCommandPoolPageSize", 0);
     corePairs->set("UserCommandPoolType", 0);
  }


  corePairs->set("numberOfScheduledMonitoringActions",dbApplication->get_MonitoringSchedule().size());
  for (unsigned int ix=0; ix < dbApplication->get_MonitoringSchedule().size(); ix++) {
     const daq::df::ScheduledMonitoringAction* sma=dbApplication->get_MonitoringSchedule()[ix];
     corePairs->set("monitoringScheduleNames", sma->get_Name(), ix);
     corePairs->set("monitoringScheduleIntervals", sma->get_Interval(), ix);
  }

  Config::T_ConfigContainer coreConfig;
  coreConfig.push_back(corePairs);

  //  corePairs->dump();
  return coreConfig;
}

//---------------------------------------------------------------

/** Shared library entry point */
extern "C" {
  extern IOManagerConfig* createROSDBConfig(void* rosName);
}

IOManagerConfig* createROSDBConfig(void* rosName) {
  return (new ROSDBConfig((char*) rosName));
}
