// -*- c++ -*-
/* Implementation of Local Controller Configuration
     Authors: G. Lehmann
     Date: %Date%
*/

#ifndef ROSDBCONFIG_H
#define ROSDBCONFIG_H

#include <string>
#include <vector>
#include "ROSCore/IOManagerConfig.h"
#include "DFThreads/DFCountedPointer.h"
#include "DFSubSystemItem/SSIConfiguration.h"

#include "dal/Partition.h"
#include "dal/HW_Object.h"
#include "dal/Application.h" 



// Run parameters via IS
#include <is/inforeceiver.h>
#include <is/infodictionary.h>
#include <rc/RunParams.h>

namespace daq {
   namespace df {
      class ReadoutApplication;
      class ReadoutTrigger;
      class ReadoutModule;
      class ReadoutOutput;
      class InputChannel;
      class MemoryPool;
   }
   namespace core {
      class ResourceBase;
      class ResourceSet;
   }
}
namespace ROS {
   class ROSDBConfig : public IOManagerConfig {
   public:
      ROSDBConfig(char * IOManagerName);

      ROSDBConfig(const ROSDBConfig&)=delete;
      ROSDBConfig& operator=(const ROSDBConfig&)=delete;

      virtual ~ROSDBConfig();
      virtual SSIConfiguration * update();
      virtual SSIConfiguration * updateRun();

      virtual Configuration* getConfiguration();
      virtual const daq::core::Partition* getPartition();
   protected:
      // Auxiliary methods
      void loadDB();  
      void getMemConfig(const daq::df::MemoryPool * memo, DFCountedPointer<Config> pairs);
      virtual SSIConfiguration * getIOManager(const daq::df::ReadoutApplication * app);
      virtual DFCountedPointer<Config> getTrigger(const daq::df::ReadoutTrigger * trg);
      virtual Config::T_ConfigContainer getOutput(std::vector<const daq::df::ReadoutOutput*> dbOut);
      virtual void getOutputChannel(const daq::df::ReadoutOutput * pOut, DFCountedPointer<Config> outPairs);
      virtual void getModule(const daq::df::ReadoutModule* rom, DFCountedPointer<Config> fmPairs);
      virtual void appendToModule(int index, const daq::df::InputChannel* channel, DFCountedPointer<Config> fmPairs);
      virtual Config::T_ConfigContainer getCore(const daq::df::ReadoutApplication* iom);
      void getAllAttributes(const std::string itemClass, const std::string itemId,
                            DFCountedPointer<Config>pairs, int index=-1);

      std::vector<const daq::core::ResourceBase*> getChannels(const daq::core::ResourceSet* module);

      ConfigObject getSelectedConfig(ConfigObject& object);

      // Auxiliary data members
      std::string m_rosName;

      Configuration * m_confDB;
      const daq::core::Partition * m_dbPartition;
      int m_dbDetectorId;
      int m_rosId;

      // For IS run parameters
      IPCPartition* m_IPCPartition;
      ISInfoDictionary* m_ISDict;
      std::string m_partitionName;

      // When running interactively IS is not available
      static int s_dummyRunNumber;

      std::vector<std::string> m_globalKeys;
      std::vector<std::string> m_globalValues;
   };
}
#endif //ROSDBCONFIG_H
