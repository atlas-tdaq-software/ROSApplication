/*---------------------------------------------------------
   Implementation of Local Controller Configurator for RCD
   Authors: E. Pasqualucci, W. Vandelli
   Initial implementation date: 4/3/2004

10/3/2004  Documentation added
-----------------------------------------------------------*/

#ifndef RCDCONFIG_H
#define RCDCONFIG_H

/**
 * The RCDConfig class inherits from the ROSDBConfig
 * class, overloading the methods related to the configuration
 * of the ROD Crate DAQ plugins.
 * The RCDConfig class is used as a plugin to the local controller
 * to manage the configuration of the RCD application.
 */

#include "ROSApplication/ROSDBConfig.h"

namespace ROS {
   class RCDConfig : public ROSDBConfig {
   public:
      /**
       * Default constructor. 
       */

      RCDConfig (char * name);

      /**
       * Destructor.
       */

      virtual ~RCDConfig () {};

   protected:
  
      // Auxiliary methods

      /**
       * Load the type and configuration of RCD Output device
       */

      virtual void getOutputChannel(const daq::df::ReadoutOutput * pOut,
                                    DFCountedPointer<Config> outPairs);

      /**
       * Load the type and configuration of the RCD input modules.
       * The configuration parameters are stored in fmPairs objects.
       */

      virtual void getModule(const daq::df::ReadoutModule* rom,
                             DFCountedPointer<Config> fmPairs);

      /**
       * Load the type and configuration of the RCD trigger.
       * The configuration parameters are stored a Config objects.
       */

      DFCountedPointer<Config> getTrigger (const daq::df::ReadoutTrigger * trg);
   };
}
#endif // RCDCONFIG_H
