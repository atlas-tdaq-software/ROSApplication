// -*- c++ -*-
// $Id$ 
//
// $Log$
// Revision 1.4  2008/11/13 11:47:01  gcrone
// Add exception specific clone and get_uid
//
// Revision 1.3  2008/04/03 11:13:01  gcrone
// Add BAD_AFFINITY error code
//
// Revision 1.2  2008/03/08 19:15:02  gcrone
// Add OBSOLETE error code
//
// Revision 1.1  2007/03/27 12:40:34  gcrone
// Initial version.
//
//
#ifndef APPLICATIONEXCEPTION_H
#define APPLICATIONEXCEPTION_H

#include "DFExceptions/ROSException.h"
namespace ROS {
  class ApplicationException : public ROSException {    
  public:
    enum ErrorCode { UNCLASSIFIED, 
		     BAD_CONFIGURATION,
                     IPC_CREATE_FAIL,
                     ISDICTIONARY_CREATE_FAIL,
                     NO_PARTITION,
                     NOT_IN_DB,
                     REL_NOT_IN_DB,
                     NOT_IN_IS,
                     ALREADY_LOADED,
                     LOAD_FAILURE,
                     BAD_ATTRIBUTE_TYPE,
                     MCAST_UNKNOWN,
                     MSGCONF_FAIL,
                     OVERRIDE,
                     OBSOLETE,
                     BAD_AFFINITY,
                     BAD_CMDLINE
    };
    ApplicationException(ErrorCode error, const ers::Context& context) ;
    ApplicationException(ErrorCode error, std::string description, const ers::Context& context) ;

     ApplicationException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context);
    virtual ~ApplicationException() throw () {};
  protected:
    virtual std::string getErrorString(unsigned int errorId) const;

    virtual ers::Issue * clone() const { return new ApplicationException( *this ); }
    static const char * get_uid() { return "ReadoutApplicationException"; }
    virtual const char * get_class_name() const { return get_uid(); }
  };

  inline ApplicationException::ApplicationException(ApplicationException::ErrorCode error, const ers::Context& context)
     : ROSException("ROSApplication", error, getErrorString(error), context) { }
   
  inline ApplicationException::ApplicationException(ApplicationException::ErrorCode error, std::string description, const ers::Context& context) 
     : ROSException("ROSApplication", error, getErrorString(error), description, context) { }

  inline ApplicationException::ApplicationException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context) 
     : ROSException(cause, "ROSApplication", error, getErrorString(error), description, context) { }

  inline std::string ApplicationException::getErrorString(unsigned int errorId) const {
     std::string rc;    
     switch (errorId) {  
     case UNCLASSIFIED:
        rc="Unclassified error" ;
        break;
     case BAD_CONFIGURATION:
        rc="Illegal Configuration";
        break;
     case IPC_CREATE_FAIL:
        rc="IPCPartition object not created";
        break;
     case ISDICTIONARY_CREATE_FAIL:
        rc="ROSDBConfig: ISInfoDictionary object not created";
        break;
     case NO_PARTITION:
        rc="ROSDBConfig: TDAQ_PARTITION not set or Partition object not found in the DB";
        break;
     case NOT_IN_DB:
        rc="Object not found in database";
        break;
     case REL_NOT_IN_DB:
        rc="Relationship not set in database";
        break;
     case NOT_IN_IS:
        rc="Object not found in IS";
        break;
     case ALREADY_LOADED:
        rc="Configuration already created: did not reload";
        break;
     case LOAD_FAILURE:
        rc="ROSDBConfig: DataBase not properly loaded";
        break;
     case BAD_ATTRIBUTE_TYPE:
        rc="Unhandled attribute type ";
        break;
     case MCAST_UNKNOWN:
        rc="Cannot determine which multicast group this ROS belongs to";
        break;
     case MSGCONF_FAIL:
        rc="Cannot initialise the Message Passing configuration";
        break;
     case OVERRIDE:
        rc="Overriding value from databse";
        break;
     case OBSOLETE:
        rc="Obsolete command line option";
        break;
     case BAD_AFFINITY:
        rc="Error setting processor affinity";
        break;
     case BAD_CMDLINE:
        rc="Error parsing command line";
        break;
    default:
      rc = "" ;
      break;
    }
    return rc;
  }
}

#endif
