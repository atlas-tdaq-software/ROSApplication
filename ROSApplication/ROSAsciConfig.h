/* Implementation of IOManager Configuration via asci files
     Authors: G. Lehmann
     Date: %Date%
*/

#ifndef ROSASCICONFIG_H
#define ROSASCICONFIG_H

#include <string>
#include "DFThreads/DFCountedPointer.h"
#include "DFSubSystemItem/SSIConfiguration.h"
#include "ROSCore/IOManagerConfig.h"

namespace ROS {
   class ROSAsciConfig : public IOManagerConfig {
   public:
      virtual ~ROSAsciConfig();
      virtual SSIConfiguration * update();
      virtual SSIConfiguration * updateRun();
   private:
      void loadConfig(std::string name, DFCountedPointer<Config> configuration);
   };
}
#endif //ROSASCICONFIG_H
